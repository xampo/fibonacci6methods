/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author francisco
 */
public class Implementacion5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n = 0;
        if (args.length<1 || args.length > 1)
	{	
            System.out.println("el uso correcto es: java implementacion5 <n>");
	}
        else
        {
             n =Integer.parseInt(args[0]);
	} 
        System.out.println(fibonacci(n));
    }
    
    /**
     * Fn = A^(n-1) * F1
     * @param n
     * @return 
     */
    public static int fibonacci(int n)
    {
        int[] f1 = {1,0};
        int[][] a = {{1,1},
                     {1,0}};
        pow(a,n);
        if(n<=1)
        {
            return n;
        }
        else
        {
            return pow(a,n-1)[0][0];
        }
                
    }

    private static int[][] pow(int[][] a, int n) 
    {
        int[][]aux = a;
        for (int i = 1; i < n; i++) 
        {
            aux = multiplicar2x2(aux, a);
        }
        return aux;
        
    }
    public static int[][] multiplicar2x2(int[][] m1, int[][] m2) 
    {
        int[][] resultado = new int[2][2];
        for (int i = 0; i < 2; i++) 
        {
            for (int j = 0; j < 2; j++) 
            {
               resultado[i][j] = (m1[i][0]*m2[0][j]) + (m1[i][1]*m2[1][j]) ; 
                
            }
        }
        return resultado;
    }

    
}
