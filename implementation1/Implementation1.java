/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author francisco
 */
public class Implementation1 {

    /**
     * @param args the command line arguments it must be a integer with the
     * wanted fibonacci
     */
    public static void main(String[] args) {
        int n = 0;
        if (args.length<1 || args.length > 1)
	{	
            System.out.println("el uso correcto es: java implementacion1 <n>");
	}
        else
        {
             n =Integer.parseInt(args[0]);
	} 
        System.out.println(fibonacci(n));
    }
    
    public static int fibonacci(int n)
    {
        if(n<=1)
        {
            return n;
        }
        else
        {
            return (fibonacci(n-1)+fibonacci(n-2));
        }
                
    }
    
}
