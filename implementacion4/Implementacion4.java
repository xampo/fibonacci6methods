/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 *
 * @author francisco
 */
public class Implementacion4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n = 0;
        if (args.length<1 || args.length > 1)
	{	
            System.out.println("el uso correcto es: java implementacion1 <n>");
	}
        else
        {
             n =Integer.parseInt(args[0]);
	} 
        System.out.println(fibonacci(n));
    }
    
    /**
     * O(log n)
     * @param n
     * @return 
     */
    public static float fibonacci(int n)
    {
        float coef = (float) (1/sqrt(5));
        float phi = (float) ((1+sqrt(5))/2);
        float phig = (float) ((1-sqrt(5))/2);
        return (float) (coef*pow(phi,n) - coef*pow(phig,n));
    }
}
