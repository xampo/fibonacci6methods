/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author francisco
 */
public class Implementacion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     int n = 0;
        if (args.length<1 || args.length > 1)
	{	
            System.out.println("el uso correcto es: java implementacion1 <n>");
	}
        else
        {
             n =Integer.parseInt(args[0]);
	} 
        System.out.println(fibonacci(n));
    }
    /**
     * implementacion iterativa usando los ultimos 2 valores de la tabla
     * @param n
     * @return 
     */
     public static int fibonacci(int n)
    {
        /**
         * a, fibonacci anterior
         * a2, fibonacci doble anterior
         * f, fibonacci actual
         * k, contador
         */
        int a, a2, f=n, k;
        
        if(n<=1)
        {
            return n;
        }
        else
        {
            k=2; a=1; a2=0;// casos base
            while(k<=n)
            {
                f = a + a2;
                a2=a;
                a=f;
                k=k+1;
            }
            return f;
        }
                
    }
    
}
