/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author francisco
 */
public class Implementacion2 {

    /**
     * Programacion dinamica
     * @param args the command line arguments, it must be an integer with the
     * wanted fibonacci
     */
    static int[] tabla;
    static int m;
    public static void main(String[] args) {
        int n = 0;
        if (args.length<1 || args.length > 1)
	{	
            System.out.println("el uso correcto es: java implementacion1 <n>");
	}
        else
        {
             n =Integer.parseInt(args[0]);
	} 
        
        tabla = new int[n+1];
        m = -1;// last known fibonacci index
        System.out.println(fibonacci(n));
        
    }
    public static int fibonacci(int n)
    {
        if(n<=m)
        {
            return tabla[n];
        }
        else
        {
            if(n<=1)
            {
                tabla[0]=0;
                tabla[1]=1;
                m=1;    
            }
            else
            {
                tabla[n]=fibonacci(n-2) + fibonacci(n-1);
                m=n;
            }
            return tabla[n];
        }
        
        
                
    }
    
}
